import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  updateEmail,
  updatePassword,
  updateFirstName,
  updateLastName,
  updateAddress,
  updateCountry,
} from "./../../redux/actions/accountActions";
import "./../../styles/Form.css";

const initialInputState = {
  account: {
    email: localStorage.getItem("email"),
    password: "",
    repeatPassword: "",
  },
  user: {
    firstName: localStorage.getItem("firstName"),
    lastName: localStorage.getItem("lastName"),
    address: localStorage.getItem("address"),
    country: localStorage.getItem("country"),
  },
};

function AccountForm({
  selectedTab,
  updateEmail,
  updatePassword,
  updateFirstName,
  updateLastName,
  updateAddress,
  updateCountry,
}) {
  const [input, setInput] = useState(initialInputState);
  const [submitted, setSubmitted] = useState(false);
  const [showPasswordWarning, setShowPasswordWarning] = useState(false);

  useEffect(() => {
    const { password, repeatPassword } = input.account;
    if (password !== repeatPassword) setShowPasswordWarning(true);
    else setShowPasswordWarning(false);
  }, [input.account]);

  const handleAccountChange = (e) => {
    setSubmitted(false);
    setInput({
      ...input,
      account: { ...input.account, [e.target.name]: e.target.value },
    });
  };

  const handleAccountSubmit = (e) => {
    e.preventDefault();
    if (!showPasswordWarning) {
      setSubmitted(true);
      updateEmail(input.account.email);
      updatePassword(input.account.password);
      // setInput(initialInputState);
    }
  };

  const handleUserChange = (e) => {
    setSubmitted(false);
    setInput({
      ...input,
      user: { ...input.user, [e.target.name]: e.target.value },
    });
  };

  const handleUserSubmit = (e) => {
    e.preventDefault();
    setSubmitted(true);
    updateFirstName(input.user.firstName);
    updateLastName(input.user.lastName);
    updateAddress(input.user.address);
    updateCountry(input.user.country);
    // setInput(initialInputState);
  };

  return (
    <div className="form">
      {selectedTab === "left" ? (
        <form onSubmit={handleAccountSubmit} className="form__content">
          <div className="form__field">
            <label htmlFor="email">E-mail</label>
            <input
              type="email"
              name="email"
              value={input.account.email}
              onChange={handleAccountChange}
            />
          </div>
          <div className="form__field">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              value={input.account.password}
              onChange={handleAccountChange}
            />
          </div>
          <div className="form__field">
            <label htmlFor="repeatPassword">Repeat Password</label>
            <input
              type="password"
              name="repeatPassword"
              value={input.account.repeatPassword}
              onChange={handleAccountChange}
            />
          </div>
          <p className={`form__warning ${showPasswordWarning && "show"}`}>
            Passwords need to be equal!
          </p>
          <div className="form__button">
            {submitted ? (
              <p className="form__buttonUpdated">Profile Updated</p>
            ) : (
              <button type="submit">Accept Changes</button>
            )}
          </div>
        </form>
      ) : (
        <form onSubmit={handleUserSubmit} className="form__content">
          <div className="form__field">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              name="firstName"
              value={input.user.firstName}
              onChange={handleUserChange}
            />
          </div>
          <div className="form__field">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              name="lastName"
              value={input.user.lastName}
              onChange={handleUserChange}
            />
          </div>
          <div className="form__field">
            <label htmlFor="address">Address</label>
            <input
              type="text"
              name="address"
              value={input.user.address}
              onChange={handleUserChange}
            />
          </div>
          <div className="form__field">
            <label htmlFor="country">Country</label>
            <select
              name="country"
              id="country"
              value={input.user.country}
              onChange={handleUserChange}
            >
              <option value="Germany">Germany</option>
              <option value="Austria">Austria</option>
              <option value="Switzerland">Switzerland</option>
            </select>
          </div>
          <div className="form__button">
            {submitted ? (
              <p className="form__buttonUpdated">Profile Updated</p>
            ) : (
              <button type="submit">Accept Changes</button>
            )}
          </div>
        </form>
      )}
    </div>
  );
}

const mapDispatchToProps = {
  updateEmail,
  updatePassword,
  updateFirstName,
  updateLastName,
  updateAddress,
  updateCountry,
};

export default connect(null, mapDispatchToProps)(AccountForm);
