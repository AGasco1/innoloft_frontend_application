import React, { useState } from "react";
import HomeForm from "./forms/HomeForm";
import AccountForm from "./forms/AccountForm";
import CompanyForm from "./forms/CompanyForm";
import SettingsForm from "./forms/SettingsForm";
import NewsForm from "./forms/NewsForm";
import AnalyticsForm from "./forms/AnalyticsForm";
import { connect } from "react-redux";
import "./../styles/FormPicker.css";

const formsTabs = {
  account: { left: "Account Settings", right: "User Settings" },
};

function FormPicker({ selectedForm }) {
  const [selectedTab, setSelectedTab] = useState("left");

  const handleSelectTab = (tab) => {
    setSelectedTab(tab);
  };

  return (
    <div className="formPicker">
      <div className="formPicker__tabs">
        <p
          className={`formPicker__tab ${
            selectedTab === "left" && "formPicker__selectedTab"
          }`}
          onClick={() => handleSelectTab("left")}
        >
          {formsTabs[selectedForm] ? formsTabs[selectedForm].left : "Left Tab"}
        </p>
        <p
          className={`formPicker__tab ${
            selectedTab === "right" && "formPicker__selectedTab"
          }`}
          onClick={() => handleSelectTab("right")}
        >
          {formsTabs[selectedForm]
            ? formsTabs[selectedForm].right
            : "Right Tab"}
        </p>
      </div>
      {selectedForm === "home" ? (
        <HomeForm selectedTab={selectedTab} />
      ) : selectedForm === "account" ? (
        <AccountForm selectedTab={selectedTab} />
      ) : selectedForm === "company" ? (
        <CompanyForm selectedTab={selectedTab} />
      ) : selectedForm === "settings" ? (
        <SettingsForm selectedTab={selectedTab} />
      ) : selectedForm === "news" ? (
        <NewsForm selectedTab={selectedTab} />
      ) : selectedForm === "analytics" ? (
        <AnalyticsForm selectedTab={selectedTab} />
      ) : (
        <h1>error: Wrong Form Selected</h1>
      )}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    selectedForm: state.form.selected,
  };
};

export default connect(mapStateToProps, null)(FormPicker);
