import React, { useEffect } from "react";
import HomeIcon from "@material-ui/icons/Home";
import PersonIcon from "@material-ui/icons/Person";
import BusinessIcon from "@material-ui/icons/Business";
import SettingsIcon from "@material-ui/icons/Settings";
import AnnouncementIcon from "@material-ui/icons/Announcement";
import AssessmentIcon from "@material-ui/icons/Assessment";
import { connect } from "react-redux";
import { selectForm } from "./../redux/actions/formActions";
import "./../styles/Sidebar.css";

function Sidebar({ selected, selectForm }) {
  useEffect(() => {
    console.log("selected", selected);
  }, [selected]);

  const handleSelectForm = (e) => {
    selectForm(e.target.id);
  };

  return (
    <div className="sidebar">
      <ul>
        <li
          className={selected === "home" ? "sidebar__selected" : ""}
          id="home"
          onClick={handleSelectForm}
        >
          <HomeIcon id="home" /> Home
        </li>
        <li
          className={selected === "account" ? "sidebar__selected" : ""}
          id="account"
          onClick={handleSelectForm}
        >
          <PersonIcon id="account" /> My Account
        </li>
        <li
          className={selected === "company" ? "sidebar__selected" : ""}
          id="company"
          onClick={handleSelectForm}
        >
          <BusinessIcon id="company" /> My Company
        </li>
        <li
          className={selected === "settings" ? "sidebar__selected" : ""}
          id="settings"
          onClick={handleSelectForm}
        >
          <SettingsIcon id="settings" /> My Settings
        </li>
        <li
          className={selected === "news" ? "sidebar__selected" : ""}
          id="news"
          onClick={handleSelectForm}
        >
          <AnnouncementIcon id="news" /> News
        </li>
        <li
          className={selected === "analytics" ? "sidebar__selected" : ""}
          id="analytics"
          onClick={handleSelectForm}
        >
          <AssessmentIcon id="analytics" />
          Analytics
        </li>
      </ul>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    selected: state.form.selected,
  };
};

const mapDispatchToProps = {
  selectForm: selectForm,
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
