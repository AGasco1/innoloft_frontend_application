import React from "react";
import LanguageIcon from "@material-ui/icons/Language";
import EmailIcon from "@material-ui/icons/Email";
import NotificationsIcon from "@material-ui/icons/Notifications";

import "./../styles/Header.css";

function Header() {
  return (
    <div className="header">
      <div className="header__left">
        <img
          src="https://eturnity.ch/wp-content/uploads/2019/06/Energieloft-Logo-Web.png"
          alt="EnergieLoft logo"
        />
      </div>
      <div className="header__right">
        <ul className="header__list">
          <li>
            <LanguageIcon /> EN
          </li>
          <li>
            <EmailIcon />
          </li>
          <li>
            <NotificationsIcon />
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Header;
