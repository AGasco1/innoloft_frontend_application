import React from "react";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar";
import FormPicker from "./components/FormPicker";
import "./App.css";

function App() {
  return (
    <div className="app">
      <div className="app__mainContainer">
        <Header />
        <div className="app__innerContainer">
          {/* SIDEBAR */}
          <Sidebar />
          {/* FORMS */}
          <FormPicker />
        </div>
      </div>
    </div>
  );
}

export default App;
