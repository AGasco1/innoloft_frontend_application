export const EMAIL_UPDATED = "EMAIL_UPDATED";
export const PASSWORD_UPDATED = "PASSWORD_UPDATED";
export const FIRST_NAME_UPDATED = "FIRST_NAME_UPDATED";
export const LAST_NAME_UPDATED = "LAST_NAME_UPDATED";
export const ADDRESS_UPDATED = "ADDRESS_UPDATED";
export const COUNTRY_UPDATED = "COUNTRY_UPDATED";

export const updateEmail = (email) => {
  localStorage.setItem("email", email);
  return { type: EMAIL_UPDATED, payload: { email } };
};

export const updatePassword = (password) => {
  localStorage.setItem("password", password);
  return {
    type: PASSWORD_UPDATED,
    payload: { password },
  };
};

export const updateFirstName = (firstName) => {
  localStorage.setItem("firstName", firstName);
  return {
    type: FIRST_NAME_UPDATED,
    payload: { firstName },
  };
};

export const updateLastName = (lastName) => {
  localStorage.setItem("lastName", lastName);
  return {
    type: LAST_NAME_UPDATED,
    payload: { lastName },
  };
};

export const updateAddress = (address) => {
  localStorage.setItem("address", address);
  return {
    type: ADDRESS_UPDATED,
    payload: { address },
  };
};

export const updateCountry = (country) => {
  localStorage.setItem("country", country);
  return {
    type: COUNTRY_UPDATED,
    payload: { country },
  };
};
