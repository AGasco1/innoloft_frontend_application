export const FORM_SELECTED = "FORM_SELECTED";

export const selectForm = (form) => {
  return {
    type: FORM_SELECTED,
    payload: { form },
  };
};
