import { createStore, combineReducers } from "redux";
import accountReducer from "./reducers/accountReducer";
import formReducer from "./reducers/formReducer";

const store = createStore(
  combineReducers({
    form: formReducer,
    account: accountReducer,
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
