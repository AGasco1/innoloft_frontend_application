import { FORM_SELECTED } from "./../actions/formActions";

const initialState = {
  selected: "account",
};

const formReducer = (state = initialState, action) => {
  switch (action.type) {
    case FORM_SELECTED:
      return {
        ...state,
        selected: action.payload.form,
      };
    default:
      return state;
  }
};

export default formReducer;
