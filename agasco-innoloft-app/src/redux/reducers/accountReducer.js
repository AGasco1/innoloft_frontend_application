import {
  EMAIL_UPDATED,
  PASSWORD_UPDATED,
  FIRST_NAME_UPDATED,
  LAST_NAME_UPDATED,
  ADDRESS_UPDATED,
  COUNTRY_UPDATED,
} from "./../actions/accountActions";

const initialState = {
  account: {
    email: localStorage.getItem("email"),
    password: localStorage.getItem("password"),
  },
  user: {
    firstName: localStorage.getItem("firstName"),
    lastName: localStorage.getItem("lastName"),
    address: localStorage.getItem("address"),
    country: localStorage.getItem("country"),
  },
};

const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case EMAIL_UPDATED:
      return {
        ...state,
        account: {
          ...state.account,
          email: action.payload.email,
        },
      };
    case PASSWORD_UPDATED:
      return {
        ...state,
        account: {
          ...state.account,
          password: action.payload.password,
        },
      };
    case FIRST_NAME_UPDATED:
      return {
        ...state,
        user: {
          ...state.user,
          firstName: action.payload.firstName,
        },
      };
    case LAST_NAME_UPDATED:
      return {
        ...state,
        user: {
          ...state.user,
          lastName: action.payload.lastName,
        },
      };
    case ADDRESS_UPDATED:
      return {
        ...state,
        user: {
          ...state.user,
          address: action.payload.address,
        },
      };
    case COUNTRY_UPDATED:
      return {
        ...state,
        user: {
          ...state.user,
          country: action.payload.country,
        },
      };
    default:
      return state;
  }
};

export default accountReducer;
